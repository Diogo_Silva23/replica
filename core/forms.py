from django import forms

from core.models import * 

class NewTopicForm(forms.ModelForm):
    message = forms.CharField(
        widget=forms.Textarea(attrs={
            'rows':5,
            'placeholder':'What is in your mind?',
            'class':'form-control'

        }),
        max_length=4000,
        help_text='The max length of the text is 4000')

    class Meta:
        model = Topic
        fields = ['subject', 'message']

        widgets={
            'subject': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Subject'
            })
        }

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['message',  ]